// El algoritmo de compresión de un String AAAA resulta en el valor 4A. Del mismo modo, el String AAAABCC se comprime a 4AB2C.

// En este ejercicio se propone remover K letras consecutivas de una cadena de N letras y conocer la compresión más corta posible que pueda lograrse.

// Veamos unos ejemplos:
// 1- Dado S = "ABBBCCDDCCC" y K = 3 las alternativas serán:

// 1.1 Removiendo ABB, el string quedaría "BCCDDCCC" y su compresión sería B2C2D3C (length = 7)
// 1.2 Removiendo BBB, el string quedaría "ACCDDCCC" y su compresión sería A2C2D3C (length = 7)
// ...
// 1.7 Removiendo DDC, el string quedaría "ABBBCCCC" y su compresión sería A3B4C (length = 5) => Siendo ésta la compresión más corta posible.

// 2- Dado S = "AAAAAAAAAAABXXAAAAAAAAAA" la compresión más corta sería 21A (length = 3)

// 3- Dado S = "ABCDDDEFG" la compresión más corta sería ABC3DG (length = 6)

// Supuestos:
// K <= N
// S es un string sólo de letras mayúsculas

// Escribir la funcion getShorterCompression(String s, int k) que imprima la compresión más corta y su longitud.

main(List<String> args) {
  String s = "ABBBCCDDCCC"; //A3B4C
  int k = 3;
  
  getShorterCompression(s, k);

  s = "AAAAAAAAAAABXXAAAAAAAAAA"; //21A
  getShorterCompression(s, k);

  s = 'ABCDDDEFG';
  getShorterCompression(s, k); //ABC3DG // a mi me da 3DEFG
}

void getShorterCompression(String input, int k){

  String removablePattern;
  String output = input;
  Map encodedStrings = {};

  for (var i = 0; i < input.length; i++) {
    if (i + k < input.length){
      
      removablePattern = input.substring(i, i + k);
      output = output.replaceFirst(removablePattern, '');

      String compressedStrings = encode(output);
      encodedStrings[compressedStrings.length] = compressedStrings;
      output = input;
    }
  }

  printMinLengthValue(encodedStrings);

}

void printMinLengthValue(Map encodedStrings) {
  int minLength;
  String minLengthCompressedString;
  encodedStrings.forEach((key, value) {
      minLength = minLength ?? key;
      minLengthCompressedString = minLengthCompressedString ?? value;
  
      if (key <= minLength ) {
        minLength = key;
        minLengthCompressedString = value;
      }
  
  });
  
  print(minLengthCompressedString);
  print(minLength);
}

String encode(String input) {
    List output = [];
    int count = 1;
    String previousChar = input[0];

    for (var i = 1; i < input.length; i++) {

        if (input[i] != previousChar) {
            output.add("${count > 1 ? count : ''}"+previousChar);
            count = 1;
            previousChar = input[i];
        }
        else 
            count++;
    }
    output.add("${count > 1 ? count : ''}"+previousChar);

    return output.join();
}