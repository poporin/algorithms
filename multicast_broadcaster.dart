/*
   Multicast UDP broadcaster
*/
import 'dart:io';
import 'dart:async';
import 'dart:convert';

void main(List<String> args) {
  InternetAddress multicastAddress = new InternetAddress('224.1.1.1');
  int multicastPort = 6811;
  dynamic host = InternetAddress.anyIPv4;

  sender(multicastAddress, multicastPort, host);
  receiver(multicastAddress, multicastPort, host);
}

void sender(multicastAddress, multicastPort, host) async {
  RawDatagramSocket.bind(host, 0).then((RawDatagramSocket socket) {
    new Timer.periodic(new Duration(seconds: 1), (Timer t) {
      Map<String, Object> mensaje = {
        "body": {
          "text": "Hola mundo",
        },
      };

      var jsonMsg = jsonEncode(mensaje);

      print("SEND:: [${jsonMsg}]");
      socket.send('$jsonMsg'.codeUnits, multicastAddress, multicastPort);
    });
  });
}

void receiver(multicastAddress, multicastPort, host) async {
  RawDatagramSocket.bind(host, multicastPort).then((RawDatagramSocket socket) {
    socket.joinMulticast(multicastAddress);
    socket.listen((RawSocketEvent e) {
      Datagram datagram = socket.receive();
      if (datagram == null) return;

      String message = new String.fromCharCodes(datagram.data).trim();
      print(jsonDecode(message));
    });
  });
}
