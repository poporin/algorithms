import 'dart:io';

String text = "Lorem ipsum dolor sit amet consectetur adipiscing elit fusce, a sollicitudin dignissim consequat turpis himenaeos vulputate, ut non nascetur parturient mus convallis est. Mus luctus commodo facilisis quisque odio pellentesque sollicitudin volutpat nisl, quam fames fermentum neque accumsan sagittis integer risus ad pulvinar, tellus eget tristique rutrum porta dapibus aptent quis. Commodo torquent tincidunt scelerisque et sem magna quisque, etiam porta mattis purus neque dictum elementum, egestas integer natoque sodales nisi velit. Quisque sed facilisi diam molestie non dictumst litora, mattis himenaeos proin dui penatibus tempor felis, taciti nulla bibendum platea curabitur montes. Libero litora sociosqu aliquet eleifend nostra ante, dignissim mauris nascetur facilisi at, mattis malesuada vivamus rhoncus ridiculus. Curabitur turpis potenti diam aenean sed quisque habitant aliquet, sapien semper quam hendrerit taciti torquent vitae luctus, dignissim feugiat vehicula ut accumsan eros cubilia. Egestas sodales sapien nulla enim vestibulum hendrerit natoque, per platea sociosqu cum facilisis ut lectus quisque, penatibus massa semper praesent facilisi euismod. Nam justo a lacus nisi habitasse augue erat, arcu id neque vehicula aliquet dis, habitant libero sagittis dapibus mus egestas. Neque et sed lacinia egestas mattis facilisis fusce ac sodales, luctus purus donec accumsan congue interdum nam lectus, porta sociis posuere tellus vehicula venenatis vitae pulvinar. Ad litora tempor dictum quisque ultricies nullam, euismod porta accumsan etiam conubia tincidunt ornare, urna himenaeos penatibus taciti tristique. Fusce morbi nec interdum malesuada ullamcorper euismod tincidunt, est aptent eros odio aenean lacinia, tortor ligula habitant luctus tellus commodo. Eros platea tortor praesent dictum quam bibendum sed velit odio, tempor sociosqu proin sagittis torquent porta venenatis congue, a magnis eleifend commodo parturient sodales augue fames. Sem conubia placerat est rhoncus maecenas taciti blandit fringilla, nisi metus facilisis lobortis risus orci. Cras commodo mus nec magnis viverra porta penatibus et sapien sagittis, risus mauris magna iaculis ante nascetur luctus metus sociosqu, vehicula potenti orci odio nam habitant himenaeos sodales maecenas. Eleifend pulvinar nec cubilia phasellus ornare nunc at est fusce, praesent litora purus laoreet risus diam suscipit penatibus, netus in dictumst sodales integer odio aliquet sollicitudin. Dignissim aliquam volutpat faucibus velit praesent nisi litora sociosqu inceptos eget, nisl curae pulvinar at potenti justo phasellus aptent. Viverra aptent per diam eu eros iaculis fames in, eleifend suspendisse phasellus nisi vulputate posuere. Mollis consequat vehicula eleifend ligula auctor cras facilisi nunc fusce, lacus risus pellentesque tristique imperdiet urna augue diam eu, nascetur semper condimentum mi sodales himenaeos per maecenas. Cras litora per sollicitudin arcu ad vulputate, pretium quisque pharetra porta at fames, proin tempor nibh himenaeos eget. In donec proin habitasse laoreet ut tempus curabitur sollicitudin, potenti odio massa eros cum posuere sodales suscipit, commodo eu vehicula id ac ullamcorper sed.";

main(List<String> args) async {

  File rawfile = new File("rawText.txt");
  File zippedFile = new File("zipped.txt");
  File unzippedFile = new File("unzipped.txt");

  await rawfile.writeAsString(text, mode: FileMode.write);
  
  String zippedString = zip(text);
  await zippedFile.writeAsString(zippedString, mode: FileMode.write);
  
  String unzippedString = unzip(zippedString);
  await unzippedFile.writeAsString(unzippedString, mode: FileMode.write);
}

/** 
 * Cada vez que se lee un nuevo carácter se revisa el diccionario para ver si forma parte de alguna entrada previa. 
 * Todos los caracteres están inicialmente predefinidos en el diccionario así que siempre habrá al menos una coincidencia, 
 * sin embargo, lo que se busca es la cadena más larga posible. 
 * 
 * Si el carácter leído no forma parte de más de una cadena más larga, entonces se emite la más larga que se hubiera encontrado y se agrega al diccionario una 
 * entrada formada por cualquiera que hubiera sido el código previo y este nuevo código. 
 * 
 * Si el carácter leído sí forma parte de más de una cadena del diccionario, se lee un nuevo carácter para ver si la secuencia formada por el carácter previo y 
 * el nuevo es alguna de las encontradas en el diccionario. 
 * En tanto los caracteres sucesivos que se vayan leyendo ofrezcan más de una entrada posible en el diccionario, se siguen leyendo caracteres. 
 * Cuando la cadena sólo tiene una entrada en el diccionario, entonces se emite el código correspondiente a esa entrada y 
 * se incorpora al diccionario una nueva entrada que representa el último código emitido y el nuevo.
 *  
 * https://es.wikipedia.org/wiki/Formato_de_compresi%C3%B3n_ZIP
 * 
*/    
String zip(String text) {
  try {
    int code = 256;
    Map dictionary = {};
    List output = [];
    List<String> data = (text + '').split('');
    String phrase = data[0];
    String currentChar;

    for (var i = 1; i < data.length; i++) {
      currentChar = data[i];
      
      if (dictionary[phrase + currentChar] != null) { 
        phrase += currentChar;
      } else { 
        output.add(phrase.length > 1 
          ? dictionary[phrase] 
          : phrase.codeUnitAt(0));
        dictionary[phrase + currentChar] = code;
        code++;
        phrase = currentChar;
      }
    }

    output.add(phrase.length > 1 
      ? dictionary[phrase] 
      : phrase.codeUnitAt(0));
    
    for (var j = 0; j < output.length; j++) {
      output[j] = String.fromCharCode(output[j]);
    }

    return output.join('');
    
  } catch (e) {
    print('Error al comprimir. ${e}');
    return '';
  }
}


String unzip(String zippedString) {
  try {
    int code = 256;
    Map dictionary = {};
    List<String> data = (zippedString + '').split('');
    String currentChar = data[0];
    List output = [currentChar];
    String phrase;
    String oldPhrase = currentChar;

    for (var i = 1; i < data.length; i++) {
      int currCode = data[i].codeUnitAt(0);
      if (currCode < 256) {
        phrase = data[i];
      } else {
        phrase = dictionary[currCode] is String 
          ? dictionary[currCode] 
          : oldPhrase + currentChar;
      }
      output.add(phrase);
      
      currentChar = phrase[0];
      dictionary[code] = oldPhrase + currentChar;
      code++;
      oldPhrase = phrase;
    }

    return output.join('');

  } catch (e) {
    print('Error al descrompimir. ${e}');
    return '';
  }
}
